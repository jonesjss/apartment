package br.com.caelum.apartment.listeners.local;

import br.com.caelum.apartment.domain.CreatedApartment;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(Source.class)
public class ApartmentListener {

    private  final MessageChannel channel;

    public ApartmentListener(@Output(Source.OUTPUT) MessageChannel channel) {
        this.channel = channel;
    }

    @EventListener
    void createdHandler(CreatedApartment event) {

        Message<CreatedApartment> message = MessageBuilder.withPayload(event).build();

        channel.send(message);
    }

}
