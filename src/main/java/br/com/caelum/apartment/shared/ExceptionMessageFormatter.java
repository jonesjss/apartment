package br.com.caelum.apartment.shared;

import lombok.AllArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.function.Function;

@AllArgsConstructor
public class ExceptionMessageFormatter<T extends Throwable> {

    private final String pattern;
    private final Class<T> exception;

    public T buildException(Object...  values) {
        String message = MessageFormat.format(pattern, values);

        try {
            Constructor<T> constructor = exception.getConstructor(String.class);
            return constructor.newInstance(message);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends Throwable> ExceptionMessageFormatter<T> template(String pattern, Class<T> exception) {
        return new ExceptionMessageFormatter<>(pattern, exception);
    }
}
