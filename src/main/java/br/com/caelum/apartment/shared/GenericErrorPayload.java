package br.com.caelum.apartment.shared;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class GenericErrorPayload {
    private final LocalDateTime date = LocalDateTime.now();
    private String error;
}
