package br.com.caelum.apartment.shared;

public interface Mapper<S, T> {
    T map(S input);
}
