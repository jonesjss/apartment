package br.com.caelum.apartment.infra.rest;

import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
class BuildingFallback implements BuildingClient {
    @Override
    public Building findById(Long id) {
        return createUnknownBuildingFor(id);
    }
    @Override
    public List<Building> findAllWhereIdsIn(List<Long> buildingIds) {
        return buildingIds.stream().map(this::createUnknownBuildingFor).collect(toList());
    }

    private Building createUnknownBuildingFor(Long id) {
        return new Building(id, "Unknown");
    }

}
