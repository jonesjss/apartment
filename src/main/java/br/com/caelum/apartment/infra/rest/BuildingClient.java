package br.com.caelum.apartment.infra.rest;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${services.building}", fallback = BuildingFallback.class)
public interface BuildingClient {

    @GetMapping("{id}")
    Building findById(@PathVariable Long id);

    @GetMapping
    List<Building> findAllWhereIdsIn(@RequestParam List<Long> buildingIds);


    @Getter
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @AllArgsConstructor
    @ToString
    class Building {
        private Long id;
        private String condominiumName;
    }

}
