package br.com.caelum.apartment.infra.repositories;

import br.com.caelum.apartment.domain.Apartment;
import br.com.caelum.apartment.api.describe.DescribreRepository;
import br.com.caelum.apartment.api.registration.RegistrationRepository;
import org.springframework.data.repository.Repository;

interface ApartmentRepository extends Repository<Apartment, Long>, RegistrationRepository, DescribreRepository {
}
