package br.com.caelum.apartment.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
public class Apartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private Long buildingId;

    @NonNull
    private Integer number;

    @NonNull
    private String identifier;
}
