package br.com.caelum.apartment.api.describe;

import br.com.caelum.apartment.domain.Apartment;

import java.util.List;
import java.util.Optional;

public interface DescribreRepository {
    Optional<Apartment> findById(Long id);

    List<Apartment> findAll();
}
