package br.com.caelum.apartment.api.describe;

import br.com.caelum.apartment.api.describe.DescribeController.ApartmentOutput;
import br.com.caelum.apartment.domain.Apartment;
import br.com.caelum.apartment.shared.Mapper;
import org.springframework.stereotype.Component;

@Component
class ApartmentToApartmentOutput implements Mapper<Apartment, ApartmentOutput> {
    @Override
    public ApartmentOutput map(Apartment input) {
        return new ApartmentOutput(input.getId(), input.getNumber(), input.getIdentifier());
    }
}
