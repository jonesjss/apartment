package br.com.caelum.apartment.api.registration;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.net.URI;

import static org.springframework.http.ResponseEntity.created;

@RestController
@AllArgsConstructor
class RegistrationController {
    private final RegistrationService service;

    @PostMapping
    ResponseEntity<?> register(@RequestBody @Valid ApartmentInput input, UriComponentsBuilder builder) {

        Long id = service.createApartment(input);

        URI uri = builder.path("{id}").build(id);

        return created(uri).build();
    }

    @Data
    static class ApartmentInput {
        @NotNull
        @Positive
        private Long buildingId;

        @NotNull
        @Positive
        private  Integer number;

        @NotBlank
        private String identifier;
    }
}
