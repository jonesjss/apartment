package br.com.caelum.apartment.api.registration;

import br.com.caelum.apartment.domain.Apartment;
import br.com.caelum.apartment.domain.CreatedApartment;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class RegistrationService {
    private final RegistrationRepository apartments;
    private final ApartmentInputMapper mapper;
    private final ApplicationEventPublisher publisher;

    public Long createApartment(RegistrationController.ApartmentInput input) {

        Apartment apartment = mapper.map(input);

        apartments.save(apartment);

        publisher.publishEvent(new CreatedApartment(apartment.getBuildingId()));

        return apartment.getId();
    }
}
