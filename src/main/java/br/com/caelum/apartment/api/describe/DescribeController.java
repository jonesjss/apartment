package br.com.caelum.apartment.api.describe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
class DescribeController {
    private static final Logger LOG = LoggerFactory.getLogger(DescribeController.class);

    private final DescribeService service;


    @GetMapping("{id}")
    ApartmentOutput show(@PathVariable Long id) {
        return service.decribeBy(id);
    }

    @GetMapping
    List<ApartmentOutput> list() {
        return service.listAll();
    }

    @Getter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @RequiredArgsConstructor
    static class ApartmentOutput {
        @NonNull
        private final Long id;

        @NonNull
        private final Integer number;

        @NonNull
        private final String identifier;

        @Setter
        private String condominiumName;

    }
}
