package br.com.caelum.apartment.api.registration;

import br.com.caelum.apartment.domain.Apartment;

public interface RegistrationRepository {
    void save(Apartment apartment);
}
