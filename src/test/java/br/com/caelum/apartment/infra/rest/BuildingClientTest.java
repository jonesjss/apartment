package br.com.caelum.apartment.infra.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerPort;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureStubRunner(
        ids = "br.com.caelum:building:+:stubs:8000",
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class BuildingClientTest {

    @Test
    void shouldGetBuildingOutput() {
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.addIfAbsent(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        RequestEntity<MultiValueMap<String, Object>> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create("http://localhost:8000/1?correlationalId=1"));

        ResponseEntity<BuildingClient.Building> output = template.exchange(request, BuildingClient.Building.class);

        assertNotNull(output);

        BuildingClient.Building building = output.getBody();

        assertEquals(1L, building.getId());
        assertEquals("Santa Julia", building.getCondominiumName());
    }
}