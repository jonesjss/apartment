package br.com.caelum.apartment.infra.rest;

import br.com.caelum.apartment.domain.CreatedApartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.context.ApplicationEventPublisher;

@SpringBootTest
@AutoConfigureMessageVerifier
public class BaseContract {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void fireEvent() {
        publisher.publishEvent(new CreatedApartment(35L));
    }
}
